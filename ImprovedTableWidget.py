# -*- coding: utf-8 -*-
import numpy as np
#from ..Qt import QtGui, Qt
from PyQt5 import QtCore, QtGui, uic
#from ..python2_3 import asUnicode, basestring
#from .. import metaarray
from pyqtgraph.widgets.TableWidget import TableWidget, TableWidgetItem
import pyqtgraph


class ImprovedTableWidgetItem(TableWidgetItem):
    def __init__(self, *args, **kwds):
        TableWidgetItem.__init__(self, *args, **kwds)

    def setCheckable(self, checkable):
        if checkable:
            self.setFlags(self.flags() | QtCore.Qt.ItemIsUserCheckable)
        else:
            self.setFlags(self.flags() & ~QtCore.Qt.ItemIsUserCheckable)


class ImprovedTableWidget(TableWidget):
    def __init__(self, *args, **kwds):
        TableWidget.__init__(self, *args, **kwds)
        self.itemClass = ImprovedTableWidgetItem
        self.itemChangedFcn = None

    def save(self, data):
        fileName = QtGui.QFileDialog.getSaveFileName(self, "Save As..", "", "Tab-separated values (*.tsv)")
        if fileName == '':
            return
        open(fileName[0], 'w+').write(data)

    def handleItemChanged(self, item):
        item.itemChanged()
        if callable(self.itemChangedFcn):
            self.itemChangedFcn()

    def data(self):
        rows = list(range(self.rowCount()))
        columns = list(range(self.columnCount()))

        data = []
        for r in rows:
            row = []
            for c in columns:
                item = self.item(r, c)
                if item is not None:
                    try:
                        row.append(float(item.value))
                    except ValueError:
                        row.append(0.0)
                else:
                    row.append(0)
            data.append(row)

        return np.array(data)

    def replace_data(self, new_data, pos=(0,0)):
        data = self.data()
        if isinstance(new_data, list):
            # if lists in list are shorter than the longest, add None elements
            length = len(sorted(new_data, key=len, reverse=True)[0])
            new_data = np.array([xi + [None] * (length - len(xi)) for xi in new_data])

        # add columns to table data, if necessary, and remove superfluous rows from clipboard data
        if np.size(new_data, 1) + pos[1] > self.columnCount():
            n_new_columns = np.size(new_data, 1) + pos[1] - self.columnCount()
            data = np.column_stack([data, np.zeros([np.size(data, 0), n_new_columns])])
        if np.size(new_data, 0) + pos[0] > self.rowCount():
            new_data = new_data[0:np.size(data, 0) - pos[0]]

        data[pos[0]:pos[0] + np.size(new_data, 0), pos[1]:pos[1] + np.size(new_data, 1)] = new_data
        self.setData(data)

    def setRow(self, row, vals):
        if row > self.rowCount() - 1:
            self.setRowCount(row + 1)
        if isinstance(vals[0], ImprovedTableWidgetItem):
            for col in range(len(vals)):
                # val = vals[col]
                item = vals[col]
                item.index = row
                self.items.append(item)
                self.setItem(row, col, item)
                # item.setValue(val)
        else:
            super().setRow(row, vals)

    def updateData(self, data):
        rows = list(range(self.rowCount()))
        columns = list(range(self.columnCount()))

        for r in rows:
            for c in columns:
                self.item(r, c).setValue(data[r, c])

    @staticmethod
    def createItems(vals, editable=True, checkable=False):
        items = []
        for col in range(len(vals)):
            val = vals[col]
            item = ImprovedTableWidgetItem(val, 0)
            item.setEditable(editable)
            item.setCheckable(checkable)
            item.setSortMode(None)
            items.append(item)
        return items

    def keyPressEvent(self, ev):
        if ev.key() == QtCore.Qt.Key_C and ev.modifiers() == QtCore.Qt.ControlModifier:
            ev.accept()
            self.copySel()
        elif ev.key() == QtCore.Qt.Key_V and ev.modifiers() == QtCore.Qt.ControlModifier:
            clipboard_data = QtGui.QApplication.clipboard().text()
            try:
                l = clipboard_data.split("\n")
                data = [i.split("\t") for i in l]
                pos = (self.currentRow(), self.currentColumn())
                self.replace_data(data,pos)

                # re-apply table formatting (TODO better)
                self.setVerticalHeaderLabels(['duration', 'gas 1', 'gas 2', 'gas 3', 'gas 4', 'gas 5'])
                duration_rowcolor = pyqtgraph.mkColor(200, 200, 200)
                for i in range(self.columnCount()):
                    self.item(0, i).setBackground(duration_rowcolor)
            except:
                pass
        else:
            QtGui.QTableWidget.keyPressEvent(self, ev)


class CheckBoxDelegate(QtGui.QStyledItemDelegate):
    """
    A delegate that places a fully functioning QCheckBox in every
    cell of the column to which it's applied
    """
    def __init__(self, parent):
        QtGui.QItemDelegate.__init__(self, parent)

    def createEditor(self, parent, option, index):
        '''
        Important, otherwise an editor is created if the user clicks in this cell.
        ** Need to hook up a signal to the model
        '''
        return None

    def paint(self, painter, option, index):
        '''
        Paint a checkbox without the label.
        '''

        if isinstance(index.data(), bool):
            checked = index.data() #index.model().data(index, QtCore.Qt.DisplayRole) == 'True'
        else:
            checked = (index.data() == 'True')
        check_box_style_option = QtGui.QStyleOptionButton()

        # if (index.flags() & QtCore.Qt.ItemIsEditable) > 0:
        #     check_box_style_option.state |= QtGui.QStyle.State_Enabled
        # else:
        #     check_box_style_option.state |= QtGui.QStyle.State_ReadOnly

        if checked:
            check_box_style_option.state |= QtGui.QStyle.State_On
        else:
            check_box_style_option.state |= QtGui.QStyle.State_Off

        check_box_style_option.rect = self.getCheckBoxRect(option)

        # this will not run - hasFlag does not exist
        #if not index.model().hasFlag(index, QtCore.Qt.ItemIsEditable):
            #check_box_style_option.state |= QtGui.QStyle.State_ReadOnly

        check_box_style_option.state |= QtGui.QStyle.State_Enabled

        QtGui.QApplication.style().drawControl(QtGui.QStyle.CE_CheckBox, check_box_style_option, painter)

    def editorEvent(self, event, model, option, index):
        '''
        Change the data in the model and the state of the checkbox
        if the user presses the left mousebutton or presses
        Key_Space or Key_Select and this cell is editable. Otherwise do nothing.
        '''
        # if not (index.flags() & QtCore.Qt.ItemIsEditable) > 0:
        #     return False

        # Do not change the checkbox-state
        if event.type() == QtCore.QEvent.MouseButtonPress:
          return False
        if event.type() == QtCore.QEvent.MouseButtonRelease or event.type() == QtCore.QEvent.MouseButtonDblClick:
            if event.button() != QtCore.Qt.LeftButton or not self.getCheckBoxRect(option).contains(event.pos()):
                return False
            if event.type() == QtCore.QEvent.MouseButtonDblClick:
                return True
        elif event.type() == QtCore.QEvent.KeyPress:
            if event.key() != QtCore.Qt.Key_Space and event.key() != QtCore.Qt.Key_Select:
                return False
            else:
                return False

        # Change the checkbox-state
        self.setModelData(None, model, index)
        return True

    def setModelData (self, editor, model, index):
        '''
        The user wanted to change the old state in the opposite.
        '''
        newValue = not index.data()
        model.setData(index, newValue, QtCore.Qt.EditRole)

    def getCheckBoxRect(self, option):
        check_box_style_option = QtGui.QStyleOptionButton()
        check_box_rect = QtGui.QApplication.style().subElementRect(QtGui.QStyle.SE_CheckBoxIndicator, check_box_style_option, None)
        check_box_point = QtCore.QPoint (option.rect.x() +
                            option.rect.width() / 2 -
                            check_box_rect.width() / 2,
                            option.rect.y() +
                            option.rect.height() / 2 -
                            check_box_rect.height() / 2)
        return QtCore.QRect(check_box_point, check_box_rect.size())