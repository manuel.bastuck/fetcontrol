import math
from func import *


class PID(object):
    def __init__(self):
        self.Kp = 1.0
        self._Ti = float('inf')
        self.Td = 0
        self.setpoint = 0.0
        self.acc_error = 0
        self.windup_bounds = (-float('inf'), float('inf'))

    def config(self):
        conf = {
            'Kp': self.Kp,
            'Ti': self.Ti,
            'Td': self.Td,
            'windup_min': self.windup_bounds[0],
            'windup_max': self.windup_bounds[1],
        }
        return conf

    def to_json(self, file=None):
        conf = {
            'Kp': self.Kp,
            'Ti': self.Ti,
            'Td': self.Td,
            'windup_min': self.windup_bounds[0],
            'windup_max': self.windup_bounds[1],
        }

        if file:
            with open(file) as f:
                json.dump(conf, f)
        else:
            return json.dumps(conf)

    def from_json(self, s='', file=None):
        if file:
            with open(file, 'r') as f:
                values = json.load(f)
        else:
            values = json.loads(s)
        self.from_save(values)

    def from_save(self, values):
        self.Kp = values['Kp']
        self.Ti = values['Ti']
        self.Td = values['Td']
        self.windup_min = values['windup_min']
        self.windup_max = values['windup_max']

    @property
    def Ti(self):
        return self._Ti

    @Ti.setter
    def Ti(self, value):
        self.acc_error = 0
        self._Ti = value

    def next_value(self, meas_val, time_step):
        e = float(self.setpoint) - float(meas_val)
        if (self.Ti == 0) or (self.Ti is float('inf')):
            self.acc_error = 0
            return self.Kp * (e + e / time_step * self.Td)
        else:
            self.acc_error += e
            self.acc_error = clamp(self.windup_bounds[0], self.acc_error, self.windup_bounds[1])
            return self.Kp * (e + 1.0/self.Ti * self.acc_error + e / time_step * self.Td)



if __name__ == '__main__':
    pid = PID()
    pid.setpoint = 0
    print(pid.next_value(1, 1))