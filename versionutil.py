import g
import func
import json
import comm
import struct
import time


def setup():
    # load board parameters globally
    with open('board_parameters.json', 'r') as f:
        g.available_boards = json.load(f)

    # identify connected boards and set globally
    boards = []
    g.available_com_ports = func.serial_ports_with_boards()
    for c in g.available_com_ports:
        b = comm.Board()
        b.com = c[0]
        b.boardid = c[1]
        b.parameters = g.available_boards[c[1]]
        boards.append(b)
    if len(boards) == 0:
        raise Exception('No boards found.')
    return boards


boards = setup()

# print('\t\trec. name\t\tport\tread id\tmaj. v.\tmin. v.')
for i, b in enumerate(boards):
    b.open()
    idv = b.hw_get_id()

    b.hw_set_relais(constant='voltage')
    b.hw_set_drainsource_voltage(2)
    #b.hw_set_drain_current(2)
    time.sleep(1)
    print(b.hw_get_drain_current())
    # print(b.hw_get_drainsource_voltage())
    try:
        idv = b.hw_get_id()
    except:
        print('Unknown board!')
        id = input('id: ')
        major_version = input('major version: ')
        minor_version = input('minor version: ')

        id = int(id)
        major_version = int(major_version)
        minor_version = int(minor_version)

        b.hw_set_id(id, major_version, minor_version)
        print('changed to: ', b.hw_get_id())
    print('({})\t\t{}\t{}\t{}\t\t{}\t\t{}'.format(i, b.boardid, b.com, *idv))
    b.close()

nr = input('(nr) to change: ')
nr = int(nr)

if int(nr) >= len(boards):
    raise IndexError

id = input('id: ')
major_version = input('major version: ')
minor_version = input('minor version: ')

id = int(id)
major_version = int(major_version)
minor_version = int(minor_version)

boards[nr].open()
boards[nr].hw_set_id(id, major_version, minor_version)
print('changed to: ', boards[nr].hw_get_id())
boards[nr].close()