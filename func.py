import sys
import glob
import serial
import serial.tools.list_ports
import json
import re


def make_unique(name, forbidden_names):
    if name in forbidden_names:
        new_name = name + '1'
        c = 1
        while new_name in forbidden_names:
            c += 2
            new_name = '{}{!s}'.format(name, c)
        name = new_name
    return name


# make valid variable names
clean = lambda varStr: re.sub('\W|^(?=\d)','_', varStr)


def serial_ports():
    """ Lists serial port names

        :raises EnvironmentError:
            On unsupported or unknown platforms
        :returns:
            A list of the serial ports available on the system
    """
    if sys.platform.startswith('win'):
        ports = ['COM%s' % (i + 1) for i in range(256)]
    elif sys.platform.startswith('linux') or sys.platform.startswith('cygwin'):
        # this excludes your current terminal "/dev/tty"
        ports = glob.glob('/dev/tty[A-Za-z]*')
    elif sys.platform.startswith('darwin'):
        ports = glob.glob('/dev/tty.*')
    else:
        raise EnvironmentError('Unsupported platform')

    result = []
    for port in ports:
        try:
            s = serial.Serial(port)
            s.close()
            result.append(port)
        except (OSError, serial.SerialException):
            pass
    return result


def serial_ports_with_boards():
    ports = list(serial.tools.list_ports.comports())
    with open('board_parameters.json', 'r') as f:
        board_parameters = json.load(f)
    board_ports = []
    for port in ports:
        com_id = port[2]
        match1 = re.search(r'.*VID:PID=(\d{4}):(\d{4}).*SER=([^\s]+)', com_id, re.IGNORECASE)
        match2 = re.search(r'.*VID_(\d{4})\+PID_(\d{4})\+([^\\]+)', com_id, re.IGNORECASE)
        match = match1 if match2 is None else match2
        print(match)
        if match is None:
            continue

        vid = match.group(1)
        pid = match.group(2)
        ser = match.group(3)
        if ser[-1] != 'A':
            ser = ser + 'A'
        print(vid, pid, ser)

        for k, b in board_parameters.items():
            # if (com_id == b['com_id']) or (com_id == b['com_id2']):
            if (vid == b['hwID']['VID']) \
                    and (pid == b['hwID']['PID']) \
                    and (ser == b['hwID']['SER']):
                board_ports.append((port[0], k))
    return board_ports


def clamp(minimum, x, maximum):
    return max(minimum, min(x, maximum))