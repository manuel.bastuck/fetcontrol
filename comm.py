import serial
import struct
import threading
import cycle
import queue
from threading import Timer
from timeit import default_timer as timer
import sched
import time
from func import *
import g
import json
import math


BAUD_RATE = 500000
TIMEOUT = 1
START_MSG = bytes([7, 1])


def analog_to_digital(analog_val, min_val, max_val, res):
    try:
        digital_val = int((analog_val - min_val) / (max_val - min_val) * res)
    except ValueError:
        digital_val = 0
    return clamp(0, digital_val, res)


def digital_to_analog(digital_val, min_val, max_val, res):
    if digital_val > res:
        digital_val = 0
    return 1. * digital_val / res * (max_val - min_val) + min_val


class InvalidResponseError(Exception):
    pass


def default_sensor_data():
    return {
            'time': float('nan'),
            'dsvoltage': float('nan'),
            'dsvoltage_set': float('nan'),
            'dsvoltage_cycle': 0,
            'dcurrent': float('nan'),
            'dcurrent_set': float('nan'),
            'dcurrent_cycle': 0,
            'temp': float('nan'),
            'temp_set': float('nan'),
            'temp_cycle': 0,
            'gate_set': float('nan'),
            'gate_cycle': 0,
            'bulk_set': float('nan'),
            'bulk_cycle': 0,
        }


class Board(object):
    def __getstate__(self):
        keys = ('sensor', 'pin', '_ua1000', 'boardid', 'current', 'heater', 'temp_sensor', 'relais', 'samplerate', 'cycles')
        out = {}
        for k in keys:
            out[k] = getattr(self, k)
        return out

    def __setstate__(self, state):
        self.widgets = None
        self.ser = serial.Serial()
        self.running = False
        self.last_update_time = timer()
        self.scheduler = sched.scheduler(time.perf_counter, time.sleep)
        self.t_start = 0
        self.cnt = 0
        self.sensor_data = default_sensor_data()
        self.thread = None
        self.data_handler_write_fun = []
        self.changed = None
        keys = ('sensor', 'pin', '_ua1000', 'boardid', 'current', 'heater', 'temp_sensor', 'relais', 'samplerate', 'cycles')
        for k in keys:
            setattr(self, k, state[k])
        for c in self.cycles.values():
            c.changed = self.cycle_changed
        for c in g.available_com_ports:
            self.parameters = g.available_boards[c[1]]

    def __init__(self):
        super().__init__()
        self.ser = serial.Serial()
        self.com = None
        self._parameters = {}
        self.sensor = ''
        self.pin = 0
        self._ua1000 = False
        self.boardid = ''

        self.major_version = None
        self.minor_version = None

        self.changed = None

        self.current = False
        self.running = False

        self.widgets = None

        self.last_update_time = timer()
        self.scheduler = sched.scheduler(time.perf_counter, time.sleep)
        self.t_start = 0
        self.cnt = 0
        # self.next_event = queue.Queue()

        self.heater = {
            'a': 70,
            'b': 370,
        }
        self.temp_sensor = {
            'r0': 100,
            'pt_coeff': (3.9083e-3, -5.775e-7),
            'u_ref': 2.56,
            'n_ges': 1024,
            'I': 1e-3,
            'minmax': (40, 550),
        }
        self.relais = 3
        self.sensor_data = default_sensor_data()

        self.samplerate = 100
        self.cycles = {}
        cycles = ('temp', 'gate', 'dsvoltage', 'dcurrent', 'bulk')
        for cname in cycles:
            self.cycles[cname] = self.create_default_cycle(cname)

        self.thread = None
        self.data_handler_write_fun = []

    def write_metadata(self, node):
        node.attrs['config'] = self.to_json()
        # for k, v in self.config().items():
        #     setattr(node.attrs, k, v)

    # def config(self):
    #     conf = {
    #         'com': self.ser.port,
    #         'boardid': self.boardid,
    #         'parameters': json.dumps(self._parameters),
    #         'sensor': self.sensor,
    #         'pin': self.pin,
    #         'heater': json.dumps(self.heater),
    #         'temp_sensor': json.dumps(self.temp_sensor),
    #         'samplerate': self.samplerate,
    #         'relais': self.relais,
    #     }
    #     for k, c in self.cycles.items():
    #         conf['cycle_' + k] = json.dumps(c.config())
    #     return conf

    def to_json(self, file=None):
        conf = {
            'com': self.ser.port,
            'board id': self.boardid,
            'parameters': self._parameters,
            'sensor': self.sensor,
            'pin': self.pin,
            'heater': self.heater,
            'temp sensor': self.temp_sensor,
            'samplerate': self.samplerate,
            'relais': self.relais,
            'ua1000': self._ua1000,
            'board version major': self.major_version,
            'board version minor': self.minor_version
        }
        for k, c in self.cycles.items():
            conf['cycle ' + k] = json.loads(c.to_json())

        if file:
            with open(file, 'w') as f:
                json.dump(conf, f, sort_keys=True, indent=4, separators=(',', ': '))
        else:
            return json.dumps(conf, sort_keys=True, indent=4, separators=(',', ': '))

    def from_json(self, s='', file=None):
        if file:
            with open(file, 'r') as f:
                values = json.load(f)
        else:
            values = json.loads(s)
        self.from_save(values)

    def from_save(self, values):
        self.sensor = values['sensor']
        self.pin = values['pin']
        self.heater = values['heater']
        self.temp_sensor = values['temp sensor']
        self.samplerate = values['samplerate']
        self.ua1000 = values['ua1000']
        self.relais = values['relais']

        for k, c in self.cycles.items():
            c.from_save(values['cycle ' + k])

    def add_datahandler_write_fun(self, datahandler, stream, fcn):
        self.data_handler_write_fun.append({
            'datahandler': datahandler,
            'stream': stream,
            'fcn': fcn,
        })

    def remove_datahandler_write_fun(self, datahandler, stream):
        self.data_handler_write_fun = [x for x in self.data_handler_write_fun
                                       if not ((x['datahandler'] == datahandler) & (x['stream'] == stream))]

    # def remove_latest_datahandler_write_fun(self):
    #     del self.data_handler_write_fun[-1]

    @property
    def ua1000(self):
        return self._ua1000

    @ua1000.setter
    def ua1000(self, value):
        if value:
            self.parameters['dcurrent_min_read'] = self.parameters['dcurrent_1000_min_read']
            self.parameters['dcurrent_max_read'] = self.parameters['dcurrent_1000_max_read']
            self.parameters['dcurrent_min_write'] = self.parameters['dcurrent_1000_min_write']
            self.parameters['dcurrent_max_write'] = self.parameters['dcurrent_1000_max_write']
        else:
            self.parameters['dcurrent_min_read'] = self.parameters['dcurrent_500_min_read']
            self.parameters['dcurrent_max_read'] = self.parameters['dcurrent_500_max_read']
            self.parameters['dcurrent_min_write'] = self.parameters['dcurrent_500_min_write']
            self.parameters['dcurrent_max_write'] = self.parameters['dcurrent_500_max_write']
        self._ua1000 = value

    @property
    def parameters(self):
        return self._parameters

    @parameters.setter
    def parameters(self, value):
        self._parameters = value
        self.ua1000 = self._ua1000

    def open(self):
        self.ser = serial.Serial()  # open serial port
        self.ser.port = self.com
        self.ser.baudrate = BAUD_RATE
        self.ser.timeout = TIMEOUT
        try:
            self.ser.open()
        except serial.SerialException:
            print('Port blocked. Plug board out and in again.')
            return False
        # print(self.ser.name)         # check which port was really used
        try:
            idversion = self.hw_get_id()
        except InvalidResponseError:
            idversion = (0, 0, 0)
        self.major_version = idversion[1]
        self.minor_version = idversion[2]

        return True

    def close(self):
        self.deactivate()
        self.ser.close()

    def start_thread(self):
        self.t_start = time.perf_counter()
        self.scheduler.enterabs(self.t_start, 100, self.update)
        self.scheduler.run()

    def activate(self):
        success = self.open()
        tries = 0
        while True:
            if (tries == 3) or (not success):
                Exception('Could not connect to board.')
                return
            try:
                # self.hw_set_relais(constant='voltage', gate='variable', bulk='variable')
                self.hw_set_relais(raw=self.relais)
                break
            except:
                self.close()
                success = self.open()
                tries += 1
        print(tries)

        # self.hw_set_id(10,3,0)
        # print('id', self.hw_get_id())
        # return

        self.cnt = 0
        self.running = True
        # self.data_handler.start()
        self.thread = threading.Thread(target=self.start_thread)
        self.thread.start()

    def deactivate(self):
        self.running = False
        self.sensor_data = default_sensor_data()
        if self.ser.isOpen() and self.running:
            self.cnt += 1
            self.scheduler.enterabs(self.t_start + self.cnt * self.samplerate / 1000, 100, self.close)
        # self.data_handler.stop()

    def update(self):
        #now = timer()
        #print(now - self.last_update_time)
        #self.last_update_time = now
        #self.sensor_data['time'] = now

        if self.running:
            self.cnt += 1
            self.scheduler.enterabs(self.t_start + self.cnt * self.samplerate/1000, 100, self.update)

            c = self.cycles

            non_supply = c['dsvoltage']
            supply = c['dcurrent']
            supply_str = self.constant_supply()
            if supply_str == 'voltage':
                supply, non_supply = non_supply, supply
            supply_next_value = supply.next_value()
            signal_next_value = non_supply.next_value()

            temp = c['temp'].next_value()
            gate = c['gate'].next_value()
            bulk = c['bulk'].next_value()
            supply = supply_next_value
            signal = signal_next_value

            now = timer()
            # print(now - self.last_update_time)
            self.last_update_time = now
            self.sensor_data['time'] = now

            self.hw_set_all(temp=c['temp'].control_value, gate=gate, bulk=bulk, supply=supply, temp_set=temp)
            self.hw_get_all()

            self.sensor_data["_".join((self.get_signal_cycle().name, "set"))] = signal_next_value

            c['temp'].set_measured_value(self.sensor_data['temp'])
            c['dsvoltage'].set_measured_value(self.sensor_data['dsvoltage'])
            c['dcurrent'].set_measured_value(self.sensor_data['dcurrent'])

            self.sensor_data['temp_cycle'] = c['temp'].n_cycle
            self.sensor_data['gate_cycle'] = c['gate'].n_cycle
            self.sensor_data['dsvoltage_cycle'] = c['dsvoltage'].n_cycle
            self.sensor_data['dcurrent_cycle'] = c['dcurrent'].n_cycle
            self.sensor_data['bulk_cycle'] = c['bulk'].n_cycle

        # self.queue.put(self.sensor_data)
        # to_delete = []
            for d in self.data_handler_write_fun:
                # print('write')
                d['fcn'](self.sensor_data)

            # for d in to_delete:
            #     del self.data_handler_write_fun[d]
            # self.data_handler.queue.put(self.sensor_data)

    def hw_get_id(self):
        cmd = b''.join((START_MSG, b'?i'))
        self.ser.write(cmd)
        ans = self.ser.read(8)
        try:
            ans = struct.unpack('3B', ans[4:-1])
        except struct.error:
            raise InvalidResponseError()
        return ans

    def hw_set_id(self, id, major_version, minor_version):
        cmd = b''.join((START_MSG, b'!i', bytes([id, major_version, minor_version])))
        self.ser.write(cmd)
        ans = self.ser.read(6)
        ans = struct.unpack('B', ans[4:-1])[0]
        return ans == 6

    def hw_get_drain_current(self):
        cmd = b''.join((START_MSG, b'?c'))
        self.ser.write(cmd)
        # ans = self.from_hw('H', self.max_drain_current_read, 16383)
        ans = self.ser.read(7)
        ans = struct.unpack('>H', ans[4:-1])
        ans = digital_to_analog(ans[0], self.parameters['dcurrent_min_read'], self.parameters['dcurrent_max_read'], 16383)
        self.sensor_data['id'] = ans
        return ans

    def hw_set_drain_current(self, value):
        hw_value = analog_to_digital(value, self.parameters['dcurrent_min_write'], self.parameters['dcurrent_max_write'], 255)
        cmd = b''.join((START_MSG, b'!c', bytes([hw_value])))
        self.ser.write(cmd)
        ans = self.ser.read(6)
        ans = struct.unpack('B', ans[4:-1])[0]
        if ans == 6:
            self.sensor_data['uds_set'] = value
        return ans == 6

    def hw_get_drainsource_voltage(self):
        cmd = b''.join((START_MSG, b'?u'))
        self.ser.write(cmd)
        ans = self.ser.read(7)
        ans = struct.unpack('>H', ans[4:-1])
        ans = digital_to_analog(ans[0], self.parameters['dsvoltage_min_read'], self.parameters['dsvoltage_max_read'], 16383)
        self.sensor_data['uds'] = ans
        return ans

    def hw_set_drainsource_voltage(self, value):
        hw_value = analog_to_digital(value, self.parameters['dsvoltage_min_write'], self.parameters['dsvoltage_max_write'], 255)
        cmd = b''.join((START_MSG, b'!d', bytes([hw_value])))
        self.ser.write(cmd)
        ans = self.ser.read(6)
        ans = struct.unpack('B', ans[4:-1])[0]
        if ans == 6:
            self.sensor_data['uds_set'] = value
        return ans == 6

    def hw_set_gatesource_voltage(self, value):
        hw_value = analog_to_digital(value, self.parameters['gate_min_write'], self.parameters['gate_max_write'], 255)
        cmd = b''.join((START_MSG, b'!g', bytes([hw_value])))
        self.ser.write(cmd)
        ans = self.ser.read(6)
        ans = struct.unpack('B', ans[4:-1])[0]
        if ans == 6:
            self.sensor_data['ugs_set'] = value
        return ans == 6

    def hw_set_bulksource_voltage(self, value):
        hw_value = analog_to_digital(value, self.parameters['bulk_min_write'], self.parameters['bulk_max_write'], 255)
        cmd = b''.join((START_MSG, b'!b', bytes([hw_value])))
        self.ser.write(cmd)
        ans = self.ser.read(6)
        ans = struct.unpack('B', ans[4:-1])[0]
        if ans == 6:
            self.sensor_data['ubs_set'] = value
        return ans == 6

    def hw_set_temperature(self, value):
        hw_value = int((value - self.heater['a']) / self.heater['b'] * 255)
        # hw_value = analog_to_digital(temp, self.heater['a'], self.heater['a'] + self.heater['b'],2**8 - 1)
        cmd = b''.join((START_MSG, b'!t', bytes([hw_value])))
        self.ser.write(cmd)
        ans = self.ser.read(6)
        ans = struct.unpack('B', ans[4:-1])[0]
        if ans == 6:
            self.sensor_data['temp_set'] = value
        return ans == 6

    def hw_get_temperature(self):
        cmd = b''.join((START_MSG, b'?d'))
        self.ser.write(cmd)
        ans = self.ser.read(7)
        ans = struct.unpack('>H', ans[4:-1])
        ans = self._temp_from_sensor(ans[0])
        self.sensor_data['temp'] = ans
        return ans

    def _force_request(self, cmd):
        def force_send_cmd():
            while True:
                try:
                    self.ser.write(cmd)
                except serial.SerialTimeoutException:
                    self.ser.flushInput()
                    time.sleep(0.05)
                else:
                    break

        def force_get_ans():
            while True:
                ans = self.ser.read(6)
                try:
                    ans = struct.unpack('B', ans[4:-1])[0]
                except struct.error:
                    self.ser.flushInput()
                    self.ser.flushOutput()
                    time.sleep(0.05)
                    force_send_cmd()
                else:
                    break
            return ans

        force_send_cmd()
        return force_get_ans()

    def hw_set_relais(self, constant=None, gate=None, gate_short=None, bulk=None, raw=None):
        # self.ser.flushOutput()
        # self.ser.flushInput()
        value = self.relais
        if constant == 'voltage':
            value |= 0b0001 #1
        else:
            value &= 0b1110
        if gate == 'variable':
            value |= 0b0010 #2
        else:
            value &= 0b1101
        if gate_short == 'drain':
            value |= 0b0100 #4
        else:
            value &= 0b1011
        if bulk == 'variable':
            value |= 0b1000 #8
        else:
            value &= 0b0111

        if raw:
            value = raw
        cmd = b''.join((START_MSG, b'!r', bytes([value])))

        ans = self._force_request(cmd)

        if ans == 6:
            self.relais = value
        return ans == 6

    def hw_set_all(self, temp=None, gate=None, bulk=None, supply=None, temp_set=None):
        # self.ser.flushOutput()
        # self.ser.flushInput()
        if temp is None:
            temp = self.sensor_data['temp_set']
        if gate is None:
            gate = self.sensor_data['gate_set']
        if bulk is None:
            bulk = self.sensor_data['bulk_set']
        if supply is None:
            if self.constant_supply() == 'voltage':
                supply = self.sensor_data['dsvoltage_set']
            else:
                supply = self.sensor_data['dcurrent_set']
        if temp_set is None:
            temp_set = temp
        if self.constant_supply() == 'voltage':
            supply_min_write = self.parameters['dsvoltage_min_write']
            supply_max_write = self.parameters['dsvoltage_max_write']
            supply_key = 'dsvoltage_set'
        else:
            supply_min_write = self.parameters['dcurrent_min_write']
            supply_max_write = self.parameters['dcurrent_max_write']
            supply_key = 'dcurrent_set'

        if self.minor_version == 0:
            num_temp_bits = 8
            num_gate_bits = 8
            num_bulk_bits = 8
            num_supply_bits = 8
        elif self.minor_version == 1:
            num_temp_bits = 8
            num_gate_bits = 13
            num_bulk_bits = 13
            num_supply_bits = 8
        else:
            raise Exception("Unknown minor version.")

        hw_value_temp = analog_to_digital(temp, self.heater['a'], self.heater['a'] + self.heater['b'],
                                          2**num_temp_bits - 1)
        hw_value_gate = analog_to_digital(gate, self.parameters['gate_min_write'],
                                          self.parameters['gate_max_write'], 2**num_gate_bits - 1)  # 255)
        hw_value_bulk = analog_to_digital(bulk, self.parameters['bulk_min_write'],
                                          self.parameters['bulk_max_write'], 2**num_bulk_bits - 1)  #255)
        hw_value_supply = analog_to_digital(supply, supply_min_write, supply_max_write, 2**num_supply_bits - 1)

        #print([hw_value_temp, hw_value_gate, hw_value_bulk, hw_value_supply])
        #print(bytes([hw_value_temp, hw_value_gate, hw_value_bulk, hw_value_supply]))

        # if self.minor_version == 0:
        #     cmd = b''.join((START_MSG, b'!e',
        #                    bytes([hw_value_temp, hw_value_gate, hw_value_bulk, hw_value_supply])))
        # elif self.minor_version == 1:
        cmd = b''.join((START_MSG, b'!e',
                        hw_value_temp.to_bytes(math.ceil(num_temp_bits / 8), byteorder="big"),
                        hw_value_gate.to_bytes(math.ceil(num_gate_bits / 8), byteorder="big"),
                        hw_value_bulk.to_bytes(math.ceil(num_bulk_bits / 8), byteorder="big"),
                        hw_value_supply.to_bytes(math.ceil(num_supply_bits / 8), byteorder="big")))
        # else:
        #     raise Exception("Unknown minor version.")

        ans = self._force_request(cmd)

        if ans == 6:
            self.sensor_data['temp_set'] = 1. * temp_set
            self.sensor_data['gate_set'] = 1. * gate
            self.sensor_data['bulk_set'] = 1. * bulk
            self.sensor_data[supply_key] = 1. * supply
        return ans == 6

    def hw_get_all(self):
        # self.ser.flushOutput()
        # self.ser.flushInput()
        cmd = b''.join((START_MSG, b'?e'))
        try:
            self.ser.write(cmd)
        except serial.SerialTimeoutException:
            vals = {'dsvoltage': 0, 'dcurrent': 0, 'temp': 0}
            return vals
        fmt = '>4B5HB'
        ans = self.ser.read(struct.calcsize(fmt))
        while len(ans) != struct.calcsize(fmt):
            try:
                self.ser.write(cmd)
            except serial.SerialTimeoutException:
                vals = {'dsvoltage': 0, 'dcurrent': 0, 'temp': 0}
                return vals
            ans = self.ser.read(struct.calcsize(fmt))
        ans = struct.unpack(fmt, ans)[4:-1]
        vals = {
            'dsvoltage': digital_to_analog(ans[0], self.parameters['dsvoltage_min_read'], self.parameters['dsvoltage_max_read'], 16383),
            'dcurrent': digital_to_analog(ans[1], self.parameters['dcurrent_min_read'], self.parameters['dcurrent_max_read'], 16383),
            'temp': self._temp_from_sensor(ans[2])
        }
        # print('heater voltage: {:.3f} V'.format(ans[3] / 1023 * 7.34))
        self.sensor_data = {**self.sensor_data, **vals}
        # for k, v in vals.items():
        #    self.sensor_data[k] = v

        return vals

    def constant_supply(self):
        if self.relais & 1:
            return 'voltage'
        else:
            return 'current'

    def get_supply_cycle(self):
        s = self.constant_supply()
        if s == 'voltage':
            return self.cycles['dsvoltage']
        else:
            return self.cycles['dcurrent']

    def get_signal_cycle(self):
        s = self.constant_supply()
        if s == 'voltage':
            return self.cycles['dcurrent']
        else:
            return self.cycles['dsvoltage']

    def _temp_from_sensor(self, value):
        r0 = self.temp_sensor['r0']
        pt = self.temp_sensor['pt_coeff']
        tminmax = self.temp_sensor['minmax']
        I = self.temp_sensor['I']
        v = self.parameters['temp_v']
        u_ref = self.temp_sensor['u_ref']
        n_ges = self.temp_sensor['n_ges']
        r_min = r0 * (1 + pt[0] * tminmax[0] + pt[1] * tminmax[0] ** 2)
        r_max = r0 * (1 + pt[0] * tminmax[1] + pt[1] * tminmax[1] ** 2)
        n_min = r_min * I * v / u_ref * n_ges
        n_max = r_max * I * v / u_ref * n_ges
        T = (value - n_min) / (n_max - n_min) * (tminmax[1] - tminmax[0]) + tminmax[0]
        return T

    def unbind_from_gui(self):
        if self.widgets is None:
            return
        self.widgets['sensor'].editingFinished.disconnect()
        self.widgets['pin'].valueChanged.disconnect()
        self.widgets['r0'].editingFinished.disconnect()
        self.widgets['heatera'].editingFinished.disconnect()
        self.widgets['heaterb'].editingFinished.disconnect()
        self.widgets['1000uA'].stateChanged.disconnect()
        self.widgets = None

    def bind_to_gui(self, widgets):
        if self.widgets is not None:
            return
        self.widgets = widgets

        # set this board's values to all widgets
        widgets['sensor'].setText(self.sensor)
        widgets['pin'].setValue(self.pin)
        widgets['r0'].setText(str(self.temp_sensor['r0']))
        widgets['heatera'].setText(str(self.heater['a']))
        widgets['heaterb'].setText(str(self.heater['b']))
        widgets['1000uA'].setCheckState(self.ua1000)

        # connect widget events to this board's set functions
        widgets['sensor'].editingFinished.connect(self._set_sensor_gui)
        widgets['pin'].valueChanged.connect(self._set_pin_gui)
        widgets['r0'].editingFinished.connect(self._set_r0_gui)
        widgets['heatera'].editingFinished.connect(self._set_heatera_gui)
        widgets['heaterb'].editingFinished.connect(self._set_heaterb_gui)
        widgets['1000uA'].stateChanged.connect(self._set_1000ua_gui)

    def _set_sensor_gui(self):
        self.sensor = self.widgets['sensor'].text()

    def _set_pin_gui(self, val):
        self.pin = val

    def _set_r0_gui(self):
        try:
            self.temp_sensor['r0'] = float(self.widgets['r0'].text())
        except ValueError:
            self.temp_sensor['r0'] = 0

    def _set_heatera_gui(self):
        try:
            self.heater['a'] = float(self.widgets['heatera'].text())
        except ValueError:
            self.heater['a'] = 0

    def _set_heaterb_gui(self):
        try:
            self.heater['b'] = float(self.widgets['heaterb'].text())
        except ValueError:
            self.heater['b'] = 0

    def _set_1000ua_gui(self, state):
        self.ua1000 = state

    def create_default_cycle(self, type):
        c = cycle.Cycle(type)
        c.board = self
        c.changed = self.cycle_changed
        if type == 'temp':
            c.minmax = [20, 500]
            c.controls_self = True
            c.self_pid.windup_bounds = (-100, 1000000)
        elif type == 'gate':
            c.minmax = [-7, 7]
            c._value_source = cycle.VALUE_SOURCE_CONST
        elif type == 'dsvoltage':
            c.minmax = [0, 6]
            c.const_val = 4
            c._value_source = cycle.VALUE_SOURCE_CONST
        elif type == 'dcurrent':
            c.minmax = [0, 1000]
            c._value_source = cycle.VALUE_SOURCE_MEASURE
        elif type == 'bulk':
            c.minmax = [-7, 7]
            c._value_source = cycle.VALUE_SOURCE_CONST
        return c

    def cycle_changed(self):
        self.cnt += 1
        self.scheduler.enterabs(self.t_start + self.cnt * self.samplerate / 1000, 100, self.set_relais_auto)
        # self.set_relais_auto()
        if callable(self.changed):
            self.changed()

    def set_relais_auto(self):
        if not self.running:
            return
        for c in self.cycles.values():
            if c.value_source == cycle.VALUE_SOURCE_CONTROLS_SIGNAL:
                signal_controlled = True
                break
        else:
            signal_controlled = False
        values = {'gate': 'variable', 'bulk': 'variable'}
        if self.cycles['dcurrent'].value_source == cycle.VALUE_SOURCE_MEASURE:
            values['constant'] = 'voltage'
        elif self.cycles['dcurrent'].value_source != cycle.VALUE_SOURCE_MEASURE and signal_controlled:
            values['constant'] = 'voltage'
        else:
            values['constant'] = 'current'
        if (self.cycles['gate'].value_source == cycle.VALUE_SOURCE_CONST) & (self.cycles['gate'].const_val == 0):
            values['gate'] = 'fixed'
        if (self.cycles['bulk'].value_source == cycle.VALUE_SOURCE_CONST) & (self.cycles['bulk'].const_val == 0):
            values['bulk'] = 'fixed'
        # print(values)
        self.hw_set_relais(**values)

    def calibrate_heater_auto(self, max_temp=300):
        if not self.running:
            print('Board must be running for temperature calibration.')
            return

        self.cycles['temp'].controls_self = False
        vs = self.cycles['temp'].value_source
        self.cycles['temp'].value_source = 'constant'
        cv = self.cycles['temp'].const_val
        self.cycles['temp'].const_val = 0

        # find temperature offset
        prev_temp = 1000
        cnt = 0
        while True:
            new_temp = self.sensor_data['temp']
            print(new_temp)
            if abs(prev_temp - new_temp) < 3:
                cnt += 1
            else:
                cnt = 0
            if cnt > 200:
                break
            prev_temp = new_temp
            time.sleep(0.1)
        self.heater['a'] = new_temp

        # find temperature slope
        # pid = self.cycles['temp'].self_pid
        # pid.Kp = 0.05
        # pid.Ti = 2
        # pid.Td = 0
        # pid.setpoint = max_temp
        # self.cycles['temp'].controls_self = True
        # hw_value = int((max_temp - self.heater['a']) / self.heater['b'] * 255)

        self.heater['b'] = 800
        self.cycles['temp'].const_val = max_temp

        print('a done')
        prev_temp = 1000
        cnt = 0
        while True:
            new_temp = self.sensor_data['temp']
            print(new_temp)
            if (max_temp - new_temp) > 3:
                self.heater['b'] -= 2
            if (max_temp - new_temp) < -3:
                self.heater['b'] += 2
            if abs(prev_temp - new_temp) < 3:
                cnt += 1
            else:
                cnt = 0
            if cnt > 100:
                break
            prev_temp = new_temp
            time.sleep(0.1)

        print(self.heater)

        self.cycles['temp'].value_source = vs
        self.cycles['temp'].const_val = cv

        #self.cycles['temp'].controls_self = False


if __name__ == "__main__":
    b = Board()
    b.open('COM4')
    id = b.hw_get_id()

    b.hw_set_all(temp=0, gate=0, bulk=0, supply=0)

    success = b.hw_set_drainsource_voltage(.3)
    success = b.hw_set_gatesource_voltage(1)
    c = b.hw_get_drain_current()
    u = b.hw_get_drainsource_voltage()
    T = b.hw_get_temperature()

    print(b.hw_set_relais(constant='voltage', gate='variable', gate_short='drain', bulk='fixed'))
    print(b.hw_get_all())

    print(success)
    print(id)
    print(c)
    print(u)
    print(T)



    # v = list(range(100))
    # for i in v:
    #     b.hw_set_drainsource_voltage(i/100)
    #     print(b.hw_get_drain_current())

    b.close()