import json

data = {
    'Board v3#001': {
        'dcurrent_1000_max_read': 979,
        'dcurrent_1000_max_write': 989,
        'dcurrent_500_max_read': 526,
        'dcurrent_500_max_write': 527,
        'dsvoltage_max_read': 11.998,
        'dsvoltage_max_write': 12.017,
        'gate_max_write': 6.875,
        'gate_min_write': -6.987,
        'bulk_max_write': 6.875,
        'bulk_min_write': -6.955,
        'temp_v': 6.4667,
    }
}

f = open('board_parameters.json', 'w')
json.dump(data, f, sort_keys=True, indent=4, separators=(',', ': '))
f.close()
