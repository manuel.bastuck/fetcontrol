import comm
import threading
import json
import g
import func
import datahandler
import os
import time


class Project(object):
    def __init__(self, gui):
        self.gui = gui
        self.boards = []
        self.file_datahandler = datahandler.FileDataHandler(self, os.getcwd())
        self.measurement_duration = 0
        self._measurement_duration_string = '000:00:00'
        self.stop_meas_timer = None
        self.start_time = 0
        self.measurement_running = False
        self.description = ''

        self.setup()

    def setup(self):
        # load board parameters globally
        with open('board_parameters.json', 'r') as f:
            g.available_boards = json.load(f)

        # identify connected boards and set globally
        g.available_com_ports = func.serial_ports_with_boards()
        for c in g.available_com_ports:
            b = self.register_board()
            b.com = c[0]
            b.boardid = c[1]
            b.parameters = g.available_boards[c[1]]
            b.open()
            b.close()
        if len(self.boards) == 0:
            raise Exception('No boards found.')
        self.boards[0].current = True

    def cleanup(self):
        for b in self.boards:
            b.deactivate()
        self.file_datahandler.stop()

    def register_board(self):
        board = comm.Board()
        self.boards.append(board)
        return board

    def start_measurement(self):
        for b in self.boards:
            if not b.running:
                continue
            for c in b.cycles.values():
                c.reset()
            self.file_datahandler.register_stream(b.boardid, b)

        self.start_time = time.time()
        if self.gui is not None:
            self.stop_meas_timer = threading.Timer(self.measurement_duration, self.gui.stop_measurement)
        else:
            self.stop_meas_timer = threading.Timer(self.measurement_duration, self.stop_measurement)
        self.stop_meas_timer.start()
        self.file_datahandler.max_samples = self.measurement_duration * 10 + 10
        self.file_datahandler.start()
        self.measurement_running = True

    def stop_measurement(self):
        try:
            self.stop_meas_timer.cancel()
        except:
            pass
        self.file_datahandler.stop()
        self.file_datahandler.clear_streams()
        self.measurement_running = False

    def __getstate__(self):
        keys = ('measurement_duration', 'boards')
        out = {}
        for k in keys:
            out[k] = getattr(self, k)
        out['file_path'] = self.file_datahandler.file_path
        return out

    def __setstate__(self, state):
        keys = ('measurement_duration', 'boards')
        for k in keys:
            setattr(self, k, state[k])
        self.file_datahandler = datahandler.FileDataHandler(state['file_path'])
        self.stop_meas_timer = None

    def to_json(self, file=None, pretty=False):
        # conf = {
        #     'boards': [json.loads(b.to_json()) for b in self.boards]
        # }
        conf = {b.boardid: json.loads(b.to_json()) for b in self.boards}
        conf['duration'] = self._measurement_duration_string
        conf['duration seconds'] = self.measurement_duration
        conf['path'] = self.file_datahandler.file_path
        conf['description'] = self.description

        if file:
            with open(file, 'w') as f:
                json.dump(conf, f, sort_keys=True, indent=4, separators=(',', ': '))
        else:
            if pretty:
                return json.dumps(conf, sort_keys=True, indent=4, separators=(',', ': '))
            else:
                return json.dumps(conf)

    def from_json(self, s='', file=None):
        if file:
            with open(file, 'r') as f:
                values = json.load(f)
        else:
            values = json.loads(s)

        for b in self.boards:
            try:
                b.from_save(values[b.boardid])
            except KeyError:
                pass

        self.measurement_duration = values['duration seconds']
        self._measurement_duration_string = values['duration']
        self.file_datahandler.file_path = values['path']
        self.description = values['description']
        if self.gui is not None:
            self.gui.meas_duration.setText(values['duration'])
            self.gui.meas_path.setText(values['path'])
            self.gui.meas_description.setText(values['description'])

    def set_measurement_duration_from_gui(self, s):
        self._measurement_duration_string = s
        duration = s.split(':')
        duration = int(duration[0]) * 3600 + int(duration[1]) * 60 + int(duration[2])
        self.measurement_duration = duration


def find_board_parameters(boards):
    for c in g.available_com_ports:
        for b in boards:
            if b.boardid == c[1]:
                b.com = c[0]
                b.parameters = g.available_boards[c[1]]
