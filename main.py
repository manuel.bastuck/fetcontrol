import sys
from PyQt5 import QtGui, uic, QtCore
import pyqtgraph as pg
import numpy as np
import g
import func
import ImprovedTableWidget
import datahandler
import project
import time
import math

qtCreatorFile = "main.ui"
Ui_MainWindow, QtBaseClass = uic.loadUiType(qtCreatorFile)


def init_cycle_table(table):
    table.setSortingEnabled(False)
    table.setData(np.zeros((100, 3)))
    table.setEditable(True)
    table.setHorizontalHeaderLabels(('time (s)', 'start', 'stop'))


class MyApp(QtBaseClass, Ui_MainWindow):
    def __init__(self, parent=None):
        QtBaseClass.__init__(self, parent)
        Ui_MainWindow.__init__(self)

        self.setupUi(self)

        self.widgets = {
            'cycles': {
                'temp': {
                    'constant': self.temp_constant_edit,
                    'toolbox': self.temp_toolbox,
                    'cycle_table': self.temp_cycle_table,
                    'sine_bias': self.temp_sine_bias,
                    'sine_amplitude': self.temp_sine_amplitude,
                    'sine_frequency': self.temp_sine_frequency,
                    'sine_periods': self.temp_sine_periods,
                    'plateau_duration_min': self.temp_plateau_duration_min,
                    'plateau_duration_max': self.temp_plateau_duration_max,
                    'plateau_value_min': self.temp_plateau_value_min,
                    'plateau_value_max': self.temp_plateau_value_max,
                    'min': self.temp_min,
                    'max': self.temp_max,
                    'set_value': self.temp_set_value_label,
                    'measured_value': self.temp_measured_value_label,
                    'cycle_step': self.temp_cycle_step_label,
                    'kp': self.temp_pid_kp,
                    'ti': self.temp_pid_ti,
                    'td': self.temp_pid_td,
                    'self_pid_checkbox': self.temp_self_pid_checkbox,
                    'self_pid_frame': self.temp_pid_frame,
                    'self_pid_kp': self.temp_self_pid_kp,
                    'self_pid_ti': self.temp_self_pid_ti,
                    'self_pid_td': self.temp_self_pid_td,
                },
                'gate': {
                    'constant': self.gate_constant_edit,
                    'toolbox': self.gate_toolbox,
                    'cycle_table': self.gate_cycle_table,
                    'sine_bias': self.gate_sine_bias,
                    'sine_amplitude': self.gate_sine_amplitude,
                    'sine_frequency': self.gate_sine_frequency,
                    'sine_periods': self.gate_sine_periods,
                    'plateau_duration_min': self.gate_plateau_duration_min,
                    'plateau_duration_max': self.gate_plateau_duration_max,
                    'plateau_value_min': self.gate_plateau_value_min,
                    'plateau_value_max': self.gate_plateau_value_max,
                    'min': self.gate_min,
                    'max': self.gate_max,
                    'set_value': self.gate_set_value_label,
                    'measured_value': None,
                    'cycle_step': self.gate_cycle_step_label,
                    'kp': self.gate_pid_kp,
                    'ti': self.gate_pid_ti,
                    'td': self.gate_pid_td,
                },
                'dsvoltage': {
                    'constant': self.dsvoltage_constant_edit,
                    'toolbox': self.dsvoltage_toolbox,
                    'cycle_table': self.dsvoltage_cycle_table,
                    'sine_bias': self.dsvoltage_sine_bias,
                    'sine_amplitude': self.dsvoltage_sine_amplitude,
                    'sine_frequency': self.dsvoltage_sine_frequency,
                    'sine_periods': self.dsvoltage_sine_periods,
                    'plateau_duration_min': self.dsvoltage_plateau_duration_min,
                    'plateau_duration_max': self.dsvoltage_plateau_duration_max,
                    'plateau_value_min': self.dsvoltage_plateau_value_min,
                    'plateau_value_max': self.dsvoltage_plateau_value_max,
                    'min': self.dsvoltage_min,
                    'max': self.dsvoltage_max,
                    'set_value': self.dsvoltage_set_value_label,
                    'measured_value': self.dsvoltage_measured_value_label,
                    'cycle_step': self.dsvoltage_cycle_step_label,
                },
                'dcurrent': {
                    'constant': self.dcurrent_constant_edit,
                    'toolbox': self.dcurrent_toolbox,
                    'cycle_table': self.dcurrent_cycle_table,
                    'sine_bias': self.dcurrent_sine_bias,
                    'sine_amplitude': self.dcurrent_sine_amplitude,
                    'sine_frequency': self.dcurrent_sine_frequency,
                    'sine_periods': self.dcurrent_sine_periods,
                    'plateau_duration_min': self.dcurrent_plateau_duration_min,
                    'plateau_duration_max': self.dcurrent_plateau_duration_max,
                    'plateau_value_min': self.dcurrent_plateau_value_min,
                    'plateau_value_max': self.dcurrent_plateau_value_max,
                    'min': self.dcurrent_min,
                    'max': self.dcurrent_max,
                    'set_value': self.dcurrent_set_value_label,
                    'measured_value': self.dcurrent_measured_value_label,
                    'cycle_step': self.dcurrent_cycle_step_label,
                },
                'bulk': {
                    'constant': self.bulk_constant_edit,
                    'toolbox': self.bulk_toolbox,
                    'cycle_table': self.bulk_cycle_table,
                    'sine_bias': self.bulk_sine_bias,
                    'sine_amplitude': self.bulk_sine_amplitude,
                    'sine_frequency': self.bulk_sine_frequency,
                    'sine_periods': self.bulk_sine_periods,
                    'plateau_duration_min': self.bulk_plateau_duration_min,
                    'plateau_duration_max': self.bulk_plateau_duration_max,
                    'plateau_value_min': self.bulk_plateau_value_min,
                    'plateau_value_max': self.bulk_plateau_value_max,
                    'min': self.bulk_min,
                    'max': self.bulk_max,
                    'set_value': self.bulk_set_value_label,
                    'measured_value': None,
                    'cycle_step': self.bulk_cycle_step_label,
                    'kp': self.bulk_pid_kp,
                    'ti': self.bulk_pid_ti,
                    'td': self.bulk_pid_td,
                },
            },  # end cycles
            'board settings': {
                'sensor': self.sensor_lineedit,
                'pin': self.pin_spinbox,
                'r0': self.r0_lineedit,
                'heatera': self.heatera_lineedit,
                'heaterb': self.heaterb_lineedit,
                '1000uA': self.ua1000_checkbox
            }
        }

        self.project = project.Project(self)

        self.timer = QtCore.QTimer()

        self.plot_datahandler = datahandler.PlotDataHandler(self.plotWidget)

        self.meas_path.setText(self.project.file_datahandler.file_path)
        self.meas_path.editingFinished.connect(self.set_measurement_path)
        self.meas_description.editingFinished.connect(self.set_measurement_description)
        self.stop_meas_timer = None

        # configure tables
        for widgets in self.widgets['cycles'].values():
            init_cycle_table(widgets['cycle_table'])

        g.available_com_ports = func.serial_ports_with_boards()
        for b in self.boards:
            self.plot_datahandler.register_stream(b.boardid, b)
            b.changed = self.board_changed
        self.plot_datahandler.current_board_name = self.current_board().boardid
        self.current_board().bind_to_gui(self.widgets['board settings'])
        for cap, cycle in self.current_cycles().items():
            cycle.bind_to_gui(self.widgets['cycles'][cap])

        table_data = []
        for b in self.boards:
            data = (b.running, b.boardid, b.com, '{}.{}'.format(b.major_version, b.minor_version))
            table_data.append(data)

        self.board_table.setItemDelegateForColumn(0, ImprovedTableWidget.CheckBoxDelegate(self))
        self.board_table.setHorizontalHeaderLabels(('active', 'board ID', 'COM'))
        self.board_table.setSortingEnabled(False)
        self.board_table.setData(table_data)
        self.board_table.itemChanged.connect(self.board_table_changed)
        self.board_table.selectionModel().currentRowChanged.connect(self.board_table_row_selection_changed)

        self.meas_path_button.clicked.connect(self.choose_path)
        self.start_meas_button.clicked.connect(self.start_measurement)
        self.stop_meas_button.clicked.connect(self.stop_measurement)
        self.stop_meas_button.setEnabled(False)

        self.meas_duration.editingFinished.connect(self.set_measurement_duration)

        self.action_save.triggered.connect(self.save_button_clicked)
        self.action_load.triggered.connect(self.load_button_clicked)
        #self.action_scan_for_boards.triggered.connect(self.scan_for_boards_button_clicked)

        self.setup()

    def choose_path(self):
        dir = QtGui.QFileDialog.getExistingDirectory()
        self.meas_path.setText(dir)

    def load_button_clicked(self):
        d = QtGui.QFileDialog()
        d.setDefaultSuffix('json')
        file = d.getOpenFileName(filter='JSON (*.json);')
        file = file[0]
        if len(file) == 0:
            return

        for b in self.boards:
            b.close()
            if b.current:
                b.unbind_from_gui()
                b.current = False
                for c in b.cycles.values():
                    c.unbind_from_gui()
        self.project.from_json(file=file)

        self.boards[0].current = True
        self.boards[0].bind_to_gui(self.widgets['board settings'])
        for k, c in self.boards[0].cycles.items():
            print('cycle', k)
            c.bind_to_gui(self.widgets['cycles'][k])
        self.plot_datahandler.current_board_name = self.boards[0].boardid

    def save_button_clicked(self):
        d = QtGui.QFileDialog()
        d.setDefaultSuffix('json')
        file = d.getSaveFileName(filter='JSON (*.json);')
        file = file[0]
        if len(file) == 0:
            return

        self.project.to_json(file)
            # pickle.dump(self.project, f, 0)

    def scan_for_boards(self):
        pass

    @property
    def boards(self):
        return self.project.boards

    def set_measurement_duration(self):
        self.project.set_measurement_duration_from_gui(self.meas_duration.text())

    def set_measurement_path(self):
        self.project.file_datahandler.file_path = self.meas_path.text()

    def set_measurement_description(self):
        self.project.description = self.meas_description.text()

    def start_measurement(self):
        self.start_meas_button.setEnabled(False)
        self.stop_meas_button.setEnabled(True)
        self.project.start_measurement()

    def stop_measurement(self):
        self.start_meas_button.setEnabled(True)
        self.stop_meas_button.setEnabled(False)
        self.project.stop_measurement()

    def board_table_changed(self, item):
        if item.column() == 0:
            val = item.text() == 'true'
            print(val)
            if val:
                self.boards[item.row()].activate()
            else:
                self.boards[item.row()].deactivate()

    def board_table_row_selection_changed(self, new=0, prev=0):
        try:
            new = new.row()
            prev = prev.row()
        except:
            pass

        if prev > -1:
            self.boards[prev].unbind_from_gui()
            for c in self.boards[prev].cycles.values():
                c.unbind_from_gui()
            self.boards[prev].current = False
        self.boards[new].current = True
        self.boards[new].bind_to_gui(self.widgets['board settings'])
        for k, c in self.boards[new].cycles.items():
            c.bind_to_gui(self.widgets['cycles'][k])
        self.plot_datahandler.current_board_name = self.boards[new].boardid

    def setup(self):
        self.register_plot('dcurrent', 'Id', 'w')
        self.register_plot('dcurrent_set', 'Id_set', pg.mkPen(color='w', style=QtCore.Qt.DashLine))
        self.register_plot('dsvoltage', 'Uds', 'b')
        self.register_plot('dsvoltage_set', 'Uds_set', pg.mkPen(color='b', style=QtCore.Qt.DashLine))
        self.register_plot('temp', 'temp', 'r')
        self.register_plot('temp_set', 'temp_set', pg.mkPen(color='r', style=QtCore.Qt.DashLine))
        self.register_plot('gate_set', 'gate', pg.mkPen(color='g', style=QtCore.Qt.DashLine))
        self.register_plot('bulk_set', 'gate', pg.mkPen(color='y', style=QtCore.Qt.DashLine))

        self.timer.timeout.connect(self.update)

        #self.autostart()

    def autostart(self):
        self.project.boards[0].cycles['temp']._table = np.array([[24, 180, 180], [24, 210, 210], [24, 240, 240], [24, 270, 270], [24, 300, 300]])
        self.project.measurement_duration = 100
        self.project.boards[0].activate()
        self.start_measurement()
        self.project.stop_meas_timer.cancel()

    def cleanup(self):
        self.project.cleanup()
        self.plot_datahandler.stop()

    def current_board(self):
        for b in self.boards:
            if b.current:
                return b

    def current_cycles(self):
        b = self.current_board()
        return b.cycles

    def register_plot(self, key, name, pen):
        self.plot_datahandler.register_plot(key, name, pen)

    def board_changed(self):
        self.plot_cycles()

    def plot_cycles(self):
        c = self.current_cycles()
        max_steps = 0
        for cyc in c.values():
            if cyc.max_steps > max_steps:
                max_steps = cyc.max_steps
        self.cycle_plot.clear()
        c['temp'].plot(self.cycle_plot, 'r', max_steps)
        c['gate'].plot(self.cycle_plot, 'g', max_steps)
        c['dsvoltage'].plot(self.cycle_plot, 'b', max_steps)
        c['dcurrent'].plot(self.cycle_plot, 'w', max_steps)
        c['bulk'].plot(self.cycle_plot, 'y', max_steps)

    def run(self):
        self.plot_datahandler.start()
        self.timer.start(100)

    def update(self):
        b = self.current_board()
        data = b.sensor_data

        cw = self.widgets['cycles']
        c = b.cycles
        self.temp_set_value_label.setText('{:.2f} °C'.format(data['temp_set']))
        self.temp_measured_value_label.setText('{:.2f} °C'.format(data['temp']))
        self.gate_set_value_label.setText('{:.2f} V'.format(data['gate_set']))
        self.dsvoltage_set_value_label.setText('{:.2f} V'.format(data['dsvoltage_set']))
        self.dsvoltage_measured_value_label.setText('{:.2f} V'.format(data['dsvoltage']))
        self.dcurrent_set_value_label.setText('{:.2f} µA'.format(data['dcurrent_set']))
        self.dcurrent_measured_value_label.setText('{:.2f} µA'.format(data['dcurrent']))
        self.bulk_set_value_label.setText('{:.2f} V'.format(data['bulk_set']))
        cw['temp']['cycle_step'].setText('{:d}/{:d}'.format(c['temp'].n_cycle, c['temp'].n_step))
        cw['gate']['cycle_step'].setText('{:d}/{:d}'.format(c['gate'].n_cycle, c['gate'].n_step))
        cw['dsvoltage']['cycle_step'].setText('{:d}/{:d}'.format(c['dsvoltage'].n_cycle, c['dsvoltage'].n_step))
        cw['dcurrent']['cycle_step'].setText('{:d}/{:d}'.format(c['dcurrent'].n_cycle, c['dcurrent'].n_step))
        cw['bulk']['cycle_step'].setText('{:d}/{:d}'.format(c['bulk'].n_cycle, c['bulk'].n_step))

        for s in self.plot_datahandler._streams:
            if s['name'] == self.plot_datahandler.current_board_name:
                for p in self.plot_datahandler.plots:
                    p['plot'].setData(s['data'][p['key']])

        rem_time = self.project.measurement_duration - (time.time() - self.project.start_time)
        if rem_time >= 0 and self.project.measurement_running:
            rem_time_h = math.floor(rem_time / 3600)
            rem_time_m = math.floor((rem_time - rem_time_h * 3600) / 60)
            rem_time_s = math.floor((rem_time - rem_time_h * 3600 - rem_time_m * 60))
            rem_time_str = "{:02d}:{:02d}:{:02d}".format(rem_time_h, rem_time_m, rem_time_s)
        else:
            rem_time_str = "00:00:00"
        self.meas_remaining_time.setText(rem_time_str)


# Back up the reference to the exceptionhook
sys._excepthook = sys.excepthook


def my_exception_hook(exctype, value, traceback):
    # Print the error and traceback
    print(exctype, value, traceback)
    # Call the normal Exception hook after
    sys._excepthook(exctype, value, traceback)
    sys.exit(1)

# Set the exception hook to our wrapping function
sys.excepthook = my_exception_hook


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    window = MyApp()
    app.aboutToQuit.connect(window.cleanup)

    window.show()
    QtCore.QTimer.singleShot(1000, window.run)
    #window.run()

    sys.exit(app.exec_())

