import numpy as np
from func import *
import math
import pid
import random
# from PyQt5.QtCore import *


VALUE_SOURCE_CYCLE = 'cycle'
VALUE_SOURCE_CONST = 'constant'
VALUE_SOURCE_SINE = 'sine'
VALUE_SOURCE_PLATEAU = 'plateau'
VALUE_SOURCE_CONTROLS_SIGNAL = 'control sensor signal'
VALUE_SOURCE_MEASURE = 'measure'
value_sources = (VALUE_SOURCE_CYCLE, VALUE_SOURCE_CONST, VALUE_SOURCE_SINE, VALUE_SOURCE_PLATEAU, VALUE_SOURCE_CONTROLS_SIGNAL, VALUE_SOURCE_MEASURE)


def value_source_index(value_source):
    for i, vs in enumerate(value_source):
        if vs == value_source:
            return i
    return -1


# def resets_cycles(func):
#     def func_wrapper(self, *args, **kwargs):
#         out = func(self, *args, **kwargs)
#         self._reset_board_cycles()
#         return out
#     return func_wrapper


class Cycle(object):
    # def __getstate__(self):
    #     keys = ('_table', '_const_val', '_role', 'minmax', '_samplerate_ms', '_mode',
    #             'sine_bias', 'sine_amplitude', 'sine_frequency', 'sine_periods')
    #     out = {}
    #     for k in keys:
    #         out[k] = getattr(self, k)
    #     return out
    #
    # def __setstate__(self, state):
    #     keys = ('_table', '_const_val', '_role', 'minmax', '_samplerate_ms', '_mode',
    #             'sine_bias', 'sine_amplitude', 'sine_frequency', 'sine_periods')
    #     for k in keys:
    #         setattr(self, k, state[k])
    #     self.n_cycle = 0
    #     self.n_step = 0
    #     self.widgets = None
    #     self.changed = None

    def __init__(self, name):
        self._name = name
        self.board = None
        self._table = np.zeros((100, 3))
        self._const_val = 0
        self._random_val = 0
        self._random_duration = 1
        self.free_val = 0
        self.control_value = 0
        self.controls_self = False
        self.minmax = [0, 0]
        self.n_cycle = 0
        self.n_step = 0
        self._samplerate_ms = 100
        self._value_source = VALUE_SOURCE_CYCLE
        # self.max_steps = 0
        self._current_value = 0
        self._measured_value = 0
        self.pid = pid.PID()
        self.self_pid = pid.PID()
        self.sine_bias = 0
        self.sine_amplitude = 0
        self.sine_frequency = 1
        self.sine_periods = 1
        self.plateau_duration_min = 1
        self.plateau_duration_max = 1
        self.plateau_value_min = 0
        self.plateau_value_max = 0
        self.widgets = None
        self.changed = None

    def config(self):
        conf = {
            'controls_self': self.controls_self,
            'value_source': self._value_source,
            'min': self.minmax[0],
            'max': self.minmax[1],
            'const': self._const_val,
        }
        table = self._table[self._table[:, 0] > 0]
        conf['table'] = json.dumps(table.tolist())
        if self.controls_signal:
            conf['value_source_pid'] = self.pid.config()
        if self.controls_self:
            conf['self_control_pid'] = self.self_pid.config()
        return conf

    def to_json(self, file=None):
        conf = {
            'controls_self': self.controls_self,
            'value_source': self._value_source,
            'minmax': self.minmax,
            'const': self._const_val,
            'sine_bias': self.sine_bias,
            'sine_amplitude': self.sine_amplitude,
            'sine_frequency': self.sine_frequency,
            'sine_periods': self.sine_periods,
            'plateau_duration_min': self.plateau_duration_min,
            'plateau_duration_max': self.plateau_duration_max,
            'plateau_value_min': self.plateau_value_min,
            'plateau_value_max': self.plateau_value_max
        }
        table = self._table[self._table[:, 0] > 0]
        conf['table'] = table.tolist()
        # if self.controls_signal:
        conf['value_source_pid'] = json.loads(self.pid.to_json())
        # if self.controls_self:
        conf['self_control_pid'] = json.loads(self.self_pid.to_json())

        if file:
            with open(file, 'w') as f:
                json.dump(conf, f)
        else:
            return json.dumps(conf)

    def from_json(self, s='', file=None):
        if file:
            with open(file, 'r') as f:
                values = json.load(f)
        else:
            values = json.loads(s)
        self.from_save(values)

    def from_save(self, values):
        self.controls_self = values['controls_self']
        self._value_source = values['value_source']
        self.minmax = values['minmax']
        self._const_val = values['const']

        # catch values that are only available in newer save files
        if 'sine_bias' in values.keys():
            self.sine_bias = values['sine_bias']
            self.sine_amplitude = values['sine_amplitude']
            self.sine_frequency = values['sine_frequency']
            self.sine_periods = values['sine_periods']
        if 'plateau_duration_min' in values.keys():
            self.plateau_duration_min = values['plateau_duration_min']
            self.plateau_duration_max = values['plateau_duration_max']
            self.plateau_value_min = values['plateau_value_min']
            self.plateau_value_max = values['plateau_value_max']

        self._table = np.zeros((100, 3))
        if len(values['table']) > 0:
            t = np.array(values['table'])
            self._table[0:t.shape[0], :] = t
        # if 'value_source_pid' in values.keys():
        self.pid.from_save(values['value_source_pid'])
        # if 'self_control_pid' in values.keys():
        self.self_pid.from_save(values['self_control_pid'])

    def set_measured_value(self, val):
        self._measured_value = val

    @property
    def controls_signal(self):
        return self.value_source == VALUE_SOURCE_CONTROLS_SIGNAL

    @property
    def name(self):
        return self._name

    @property
    def measured_value(self):
        return self._measured_value

    @property
    def current_value(self):
        return self._current_value

    @property
    def table(self):
        return self._table

    @table.setter
    def table(self, value):
        # self._reset_board_cycles()
        self._table = value
        # self.max_steps = self._max_steps()
        if callable(self.changed):
            self.changed()

    @property
    def const_val(self):
        return self._const_val

    @const_val.setter
    def const_val(self, value):
        self._const_val = value
        if callable(self.changed):
            self.changed()

    @property
    def value_source(self):
        return self._value_source

    @value_source.setter
    def value_source(self, value):
        self._reset_board_cycles()
        self._value_source = value
        if callable(self.changed):
            self.changed()

    @property
    def samplerate_ms(self):
        return self._samplerate_ms

    @samplerate_ms.setter
    def samplerate_ms(self, value):
        self._samplerate_ms = value
        if callable(self.changed):
            self.changed()

    def reset(self):
        self.n_cycle = 0
        self.n_step = -1

    def _reset_board_cycles(self):
        for c in self.board.cycles.values():
            c.reset()

    def advance(self):
        max_steps = self._max_steps()
        if max_steps == 1:
            self.n_step = 0
            return
        self.n_step += 1
        if self.n_step >= max_steps:
            self.n_step = 0
            self.n_cycle += 1

    def set_samplerate(self, samplerate):
        self.samplerate_ms = samplerate
        # self.max_steps = self._max_steps()

    @property
    def max_steps(self):
        return self._max_steps()

    def _max_steps(self):
        if self.value_source == VALUE_SOURCE_CYCLE:
            return sum(self.table[:, 0]) / (self.samplerate_ms/1000)
        elif self.value_source == VALUE_SOURCE_SINE:
            return (self.sine_periods / self.sine_frequency) / (self.samplerate_ms/1000)
        elif self.value_source == VALUE_SOURCE_PLATEAU:
            if self.n_step == 0:
                self._random_duration = random.uniform(self.plateau_duration_min, self.plateau_duration_max)
            return self._random_duration / (self.samplerate_ms/1000)
        else:
            return 1

    def next_value(self):
        self.advance()
        val = self._cycle_value()
        if math.isnan(val):
            return val
        val = clamp(self.minmax[0], val, self.minmax[1])
        self.control_value = val
        if self.controls_self:
            self.self_pid.setpoint = val
            self.control_value = self.self_pid.next_value(self.measured_value, self.samplerate_ms / 1000)
            self.control_value = clamp(self.minmax[0], self.control_value, self.minmax[1])
        self._current_value = val
        return val

    def _cycle_value(self):
        if self.value_source == VALUE_SOURCE_CONST:
            return self.const_val
        elif self.value_source == VALUE_SOURCE_CYCLE:
            steps = self._table[:, 0] / (self.samplerate_ms/1000)
            steps_cumsum = np.cumsum(steps)
            current_row = np.argmax(self.n_step <= (steps_cumsum-1))
            if current_row == 0:
                row_steps = self.n_step
            else:
                row_steps = self.n_step - steps_cumsum[current_row-1]
            time = self._table[current_row, 0]
            start = self._table[current_row, 1]
            stop = self._table[current_row, 2]
            if steps[current_row] == 0:
                return 0
            slope = (stop - start) / (steps[current_row])
            return start + slope*(row_steps)
        elif self.value_source == VALUE_SOURCE_SINE:
            f = self.n_step / self._max_steps() * self.sine_periods * 2 * math.pi
            return math.sin(f) * self.sine_amplitude + self.sine_bias
        elif self.value_source == VALUE_SOURCE_PLATEAU:
            if self.n_step == 0:
                self._random_val = random.uniform(self.plateau_value_min, self.plateau_value_max)
            return self._random_val
        elif self.value_source == VALUE_SOURCE_CONTROLS_SIGNAL:
            self.pid.setpoint = self.board.get_signal_cycle().current_value
            val = self.pid.next_value(self.board.get_signal_cycle().measured_value, self.samplerate_ms / 1000)
            return clamp(self.minmax[0], val, self.minmax[1])
        elif self.value_source == VALUE_SOURCE_MEASURE:
            return float('nan')

    def plot(self, plotWidget, pen='w', steps=None):
        if steps is None:
            steps = self.max_steps
        if self.value_source == VALUE_SOURCE_CONST:
            plotWidget.plot([0, steps*(self.samplerate_ms/1000)], [self.const_val]*2, pen=pen)
        elif self.value_source == VALUE_SOURCE_CYCLE:
            if self.max_steps == 0:
                return
            times = self._table[:, 0]
            time_not_zero = times > 0
            times = times[time_not_zero]
            times_cumsum = np.cumsum(times)
            times_cumsum = np.repeat(times_cumsum, 2)
            times_cumsum = np.roll(times_cumsum, 1)
            times_cumsum[0] = 0
            val = self._table[time_not_zero, 1:3]
            values = np.reshape(val, (np.size(val),))
            rep = int(math.ceil(steps / self.max_steps))
            offset = np.repeat(times_cumsum[-1] * np.array(range(rep)), np.size(times_cumsum))
            times_cumsum = np.tile(times_cumsum, rep) + offset
            values = np.tile(values, rep)
            plotWidget.plot(times_cumsum, values, pen=pen)

    def unbind_from_gui(self):
        if self.widgets is None:
            return
        self.widgets['constant'].editingFinished.disconnect()
        self.widgets['toolbox'].currentChanged.disconnect()
        # self.widgets['cycle_table'].itemChanged.disconnect()
        self.widgets['cycle_table'].itemChangedFcn = None
        self.widgets['min'].editingFinished.disconnect()
        self.widgets['max'].editingFinished.disconnect()

        self.widgets['sine_bias'].editingFinished.disconnect()
        self.widgets['sine_amplitude'].editingFinished.disconnect()
        self.widgets['sine_frequency'].editingFinished.disconnect()
        self.widgets['sine_periods'].editingFinished.disconnect()

        self.widgets['plateau_duration_min'].editingFinished.disconnect()
        self.widgets['plateau_duration_max'].editingFinished.disconnect()
        self.widgets['plateau_value_min'].editingFinished.disconnect()
        self.widgets['plateau_value_max'].editingFinished.disconnect()

        try:
            self.widgets['kp'].editingFinished.disconnect()
            self.widgets['ti'].editingFinished.disconnect()
            self.widgets['td'].editingFinished.disconnect()
        except KeyError:
            pass
        try:
            self.widgets['self_pid_checkbox'].stateChanged.disconnect()
            self.widgets['self_pid_kp'].editingFinished.disconnect()
            self.widgets['self_pid_ti'].editingFinished.disconnect()
            self.widgets['self_pid_td'].editingFinished.disconnect()
        except KeyError:
            pass
        self.widgets = None

    def bind_to_gui(self, widgets):
        if self.widgets is not None:
            return
        self.widgets = widgets

        widgets['constant'].setText(str(self.const_val))
        widgets['toolbox'].setCurrentIndex(self.get_toolbox_index(widgets['toolbox'], self.value_source))
        widgets['cycle_table'].updateData(self._table)

        widgets['sine_bias'].setText(str(self.sine_bias))
        widgets['sine_amplitude'].setText(str(self.sine_amplitude))
        widgets['sine_frequency'].setText(str(self.sine_frequency))
        widgets['sine_periods'].setText(str(self.sine_periods))

        widgets['plateau_duration_min'].setText(str(self.plateau_duration_min))
        widgets['plateau_duration_max'].setText(str(self.plateau_duration_max))
        widgets['plateau_value_min'].setText(str(self.plateau_value_min))
        widgets['plateau_value_max'].setText(str(self.plateau_value_max))

        widgets['min'].setText(str(self.minmax[0]))
        widgets['max'].setText(str(self.minmax[1]))
        try:
            widgets['kp'].setText(str(self.pid.Kp))
            widgets['ti'].setText(str(self.pid.Ti))
            widgets['td'].setText(str(self.pid.Td))
        except KeyError:
            pass
        try:
            if self.controls_self:
                widgets['self_pid_checkbox'].setCheckState(2)  #Qt.Checked)
                widgets['self_pid_frame'].show()
            else:
                widgets['self_pid_checkbox'].setCheckState(0) #Qt.Unchecked)
                widgets['self_pid_frame'].hide()
            widgets['self_pid_kp'].setText(str(self.self_pid.Kp))
            widgets['self_pid_ti'].setText(str(self.self_pid.Ti))
            widgets['self_pid_td'].setText(str(self.self_pid.Td))
        except KeyError:
            pass

        widgets['constant'].editingFinished.connect(self._set_constant_gui)

        widgets['sine_bias'].editingFinished.connect(self._set_sine_bias_gui)
        widgets['sine_amplitude'].editingFinished.connect(self._set_sine_amplitude_gui)
        widgets['sine_frequency'].editingFinished.connect(self._set_sine_frequency_gui)
        widgets['sine_periods'].editingFinished.connect(self._set_sine_periods_gui)

        widgets['plateau_duration_min'].editingFinished.connect(self._set_plateau_duration_min_gui)
        widgets['plateau_duration_max'].editingFinished.connect(self._set_plateau_duration_max_gui)
        widgets['plateau_value_min'].editingFinished.connect(self._set_plateau_value_min_gui)
        widgets['plateau_value_max'].editingFinished.connect(self._set_plateau_value_max_gui)

        widgets['toolbox'].currentChanged.connect(self._set_value_source_gui)
        # widgets['cycle_table'].itemChanged.connect(self._set_table_gui)
        widgets['cycle_table'].itemChangedFcn = self._set_table_gui
        widgets['min'].editingFinished.connect(self._set_min_gui)
        widgets['max'].editingFinished.connect(self._set_max_gui)
        try:
            widgets['kp'].editingFinished.connect(self._set_kp_gui)
            widgets['ti'].editingFinished.connect(self._set_ti_gui)
            widgets['td'].editingFinished.connect(self._set_td_gui)
        except KeyError:
            pass
        try:
            widgets['self_pid_checkbox'].stateChanged.connect(self._set_controls_self_gui)
            widgets['self_pid_kp'].editingFinished.connect(self._set_self_pid_kp_gui)
            widgets['self_pid_ti'].editingFinished.connect(self._set_self_pid_ti_gui)
            widgets['self_pid_td'].editingFinished.connect(self._set_self_pid_td_gui)
        except KeyError:
            pass

    @staticmethod
    def get_toolbox_index(toolbox_widget, value_source):
        for i in range(len(value_sources)):
            item_text = toolbox_widget.itemText(i)
            # print('text', item_text)
            # print('value_source', value_source)
            if item_text == value_source:
                return i
        raise ValueError

    def _set_constant_gui(self):
        self.const_val = float(self.widgets['constant'].text())
        self._reset_board_cycles()

    def _set_sine_bias_gui(self):
        self.sine_bias = float(self.widgets['sine_bias'].text())
        self._reset_board_cycles()

    def _set_sine_amplitude_gui(self):
        self.sine_amplitude = float(self.widgets['sine_amplitude'].text())
        self._reset_board_cycles()

    def _set_sine_frequency_gui(self):
        self.sine_frequency = float(self.widgets['sine_frequency'].text())
        self._reset_board_cycles()

    def _set_sine_periods_gui(self):
        self.sine_periods = float(self.widgets['sine_periods'].text())
        self._reset_board_cycles()

    def _set_plateau_duration_min_gui(self):
        self.plateau_duration_min = float(self.widgets['plateau_duration_min'].text())
        self._reset_board_cycles()

    def _set_plateau_duration_max_gui(self):
        self.plateau_duration_max = float(self.widgets['plateau_duration_max'].text())
        self._reset_board_cycles()

    def _set_plateau_value_min_gui(self):
        self.plateau_value_min = float(self.widgets['plateau_value_min'].text())
        self._reset_board_cycles()

    def _set_plateau_value_max_gui(self):
        self.plateau_value_max = float(self.widgets['plateau_value_max'].text())
        self._reset_board_cycles()

    def _set_value_source_gui(self, i):
        self.value_source = self.widgets['toolbox'].itemText(i)
        self._reset_board_cycles()

    def _set_table_gui(self):
        self.table = self.widgets['cycle_table'].data()
        self._reset_board_cycles()

    def _set_min_gui(self):
        self.minmax[0] = float(self.widgets['min'].text())

    def _set_max_gui(self):
        self.minmax[1] = float(self.widgets['max'].text())

    def _set_kp_gui(self):
        try:
            self.pid.Kp = float(self.widgets['kp'].text())
        except ValueError:
            self.widgets['kp'].setText(str(self.pid.Kp))

    def _set_ti_gui(self):
        try:
            self.pid.Ti = float(self.widgets['ti'].text())
        except ValueError:
            self.widgets['ti'].setText(str(self.pid.Ti))

    def _set_td_gui(self):
        try:
            self.pid.Td = float(self.widgets['td'].text())
        except ValueError:
            self.widgets['td'].setText(str(self.pid.Td))

    def _set_controls_self_gui(self, *args):
        self.controls_self = self.widgets['self_pid_checkbox'].isChecked()
        if self.controls_self:
            self.widgets['self_pid_frame'].show()
        else:
            self.widgets['self_pid_frame'].hide()

    def _set_self_pid_kp_gui(self):
        try:
            self.self_pid.Kp = float(self.widgets['self_pid_kp'].text())
        except ValueError:
            self.widgets['self_pid_kp'].setText(str(self.self_pid.Kp))

    def _set_self_pid_ti_gui(self):
        try:
            self.self_pid.Ti = float(self.widgets['self_pid_ti'].text())
        except ValueError:
            self.widgets['self_pid_ti'].setText(str(self.self_pid.Ti))

    def _set_self_pid_td_gui(self):
        try:
            self.self_pid.Td = float(self.widgets['self_pid_td'].text())
        except ValueError:
            self.widgets['self_pid_td'].setText(str(self.self_pid.Td))