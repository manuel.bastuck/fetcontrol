import threading
import sched
import h5py
import queue
import time
import numpy as np
import func
from timeit import default_timer as timer
import os


class DataHandler(object):
    def __init__(self):
        # self.queue = queue.Queue()
        self._streams = []
        self.rate = 1000
        self.thread = None
        self.scheduler = sched.scheduler(time.perf_counter, time.sleep)
        self.next_event = None

    @property
    def streams(self):
        return self._streams

    def register_stream(self, name, obj):
        q = queue.Queue()
        names = [x['name'] for x in self._streams]
        name = func.make_unique(name, names)
        self._streams.append({'queue': q, 'name': name, 'object': obj})
        obj.add_datahandler_write_fun(self, name, lambda data: q.put(data))

    def clear_streams(self):
        for s in self._streams:
            s['object'].remove_datahandler_write_fun(self, s['name'])
        self._streams = []

    def start_thread(self):
        self.next_event = self.scheduler.enter(self.rate / 1000, 1, self.handle_loop)
        self.scheduler.run()

    def setup(self):
        pass

    def start(self):
        self.setup()
        self.thread = threading.Thread(target=self.start_thread)
        self.thread.start()

    def teardown(self):
        pass

    def stop(self):
        self.teardown()
        if self.next_event is not None:
            self.scheduler.cancel(self.next_event)
            self.next_event = None

    def add_data_to_stream(self, stream, data):
        stream['queue'].put(data)

    def handle_loop(self):
        self.next_event = self.scheduler.enter(self.rate / 1000, 1, self.handle_loop)
        self.handle()

    def handle(self):
        pass


class FileDataHandler(DataHandler):
    def __init__(self, project, file_path=''):
        DataHandler.__init__(self)
        self.file_handle = None
        self.file_path = file_path
        self.file_name = ''
        self.start_time = None
        self.write_metadata = None
        self.project = project
        self.rate = 30000

        self.column_datatypes = [('time', np.float32),
                                 ('dsvoltage', np.float32),
                                 ('dsvoltage_set', np.float32),
                                 ('dsvoltage_cycle', np.uint32),
                                 ('dcurrent', np.float32),
                                 ('dcurrent_set', np.float32),
                                 ('dcurrent_cycle', np.uint32),
                                 ('temp', np.float32),
                                 ('temp_set', np.float32),
                                 ('temp_cycle', np.uint32),
                                 ('gate_set', np.float32),
                                 ('gate_cycle', np.uint32),
                                 ('bulk_set', np.float32),
                                 ('bulk_cycle', np.uint32)]
        self.column_names = tuple([x[0] for x in self.column_datatypes])
        self.ncols = len(self.column_datatypes)
        self.max_samples = None

    def setup(self):
        time_string = time.strftime('%y_%m_%d_%H_%M_%S')
        self.file_name = time_string + '.h5'
        # h5file = h5py.File(name=os.path.join(self.file_path, self.file_name), mode='w', libver='latest')
        with h5py.File(name=os.path.join(self.file_path, self.file_name), mode='w', libver='earliest') as h5file:
            h5file.attrs['software'] = 'FETcontrol'
            h5file.attrs['version'] = '1802'
            h5file.attrs['starttime'] = time_string
            h5file.attrs['config'] = self.project.to_json(pretty=True)
            for s in self._streams:
                name = func.clean(s['name'])
                s['clean_name'] = name
                empty_data = np.empty((0, 1), dtype=self.column_datatypes)
                # s['table'] = h5file.create_dataset(name, data=empty_data, compression='gzip', maxshape=(self.max_samples, 1))
                h5file.create_dataset(name, data=empty_data, compression='gzip', maxshape=(self.max_samples, 1))
                # s['object'].write_metadata(s['table'])
            # self.file_handle = h5file
        self.start_time = timer()

    def teardown(self):
        self.handle(0)
        if self.file_handle is not None:
            self.file_handle.close()
        self.file_handle = None
        self.start_time = None

    def handle(self, entries_left=1):
        with h5py.File(name=os.path.join(self.file_path, self.file_name)) as h5file:
            for s in self._streams:
                result_list = []
                while s['queue'].qsize() > entries_left: # not s['queue'].empty():
                    try:
                        result_list.append(s['queue'].get(False))
                    except queue.Empty:
                        break
                    s['queue'].task_done()
                if len(result_list) == 0:
                    return
                values = [tuple([each[c] for c in self.column_names]) for each in result_list]
                data_array = np.array(values, dtype=self.column_datatypes)
                for d in data_array:
                    d['time'] = d['time'] - self.start_time
                prev_rows = h5file[s['clean_name']].shape[0] # s['table'].shape[0]
                h5file[s['clean_name']].resize(prev_rows + data_array.shape[0], 0)
                h5file[s['clean_name']][prev_rows:] = data_array[:, None]

                h5file.flush()

                #self.file_handle.flush()


class PlotDataHandler(DataHandler):
    def __init__(self, plot_widget=None):
        DataHandler.__init__(self)
        self.plotWidget = plot_widget
        self.plots = []
        self.current_board_name = ''
        self.xaxis_samplerate = 100
        self.buffer = 6000
        self._x = []
        self.rate = 100

    def register_stream(self, name, obj):
        DataHandler.register_stream(self, name, obj)
        data = {}
        for p in self.plots:
            data[p['key']] = np.zeros(self.buffer)
        self._streams[-1]['data'] = data

    def register_plot(self, key, name, pen='w'):
        self.plots.append({'key': key, 'name': name, 'pen': pen})
        for s in self._streams:
            s['data'][key] = np.zeros(self.buffer)

    def setup(self):
        self.plotWidget.clear()
        self.plotWidget.addLegend()
        self._x = np.array(list(range(0, self.buffer))) * (self.xaxis_samplerate / 1000)
        #for s in self._streams:
            #if s['name'] == self.current_board_name:
        s = self._streams[0]
        for p in self.plots:
            p['plot'] = self.plotWidget.plot(self._x, s['data'][p['key']], name=p['name'], pen=p['pen'])

    def handle(self):
        for s in self._streams:
            result_list = []
            while not s['queue'].empty():
                result_list.append(s['queue'].get())
            if len(result_list) == 0:
                continue
            for p in self.plots:
                data = s['data'][p['key']]
                data[:-len(result_list)] = data[len(result_list):]
                data[-len(result_list):] = np.array([x[p['key']] for x in result_list])
                if s['name'] == self.current_board_name:
                    pass
                    # self.call_main_update_fcn(p, data)
                    # p['plot'].setData(data)
